<?php

class Admin extends GenericsLibrary{
    use Generics_Object_Traits;

    protected string $id;
    protected string $name;
    protected int $level;
    
    public function __construct(int $c_id, string $c_name, int $c_level) {
        $this->id = $c_id;
        $this->name = $c_name;
        $this->level = $c_level;
        array_push(self::$all_instanced_objects, $this);
    }

}