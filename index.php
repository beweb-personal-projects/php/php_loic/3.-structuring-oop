<?php 
define('WEBROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_NAME']));
define('ROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));

require('data.php');
require(ROOT.'/lib/classes/generics.php');
require(ROOT.'/controllers/admin.php');

// ****************************************************************************************
// To add: getObjectById, setObjectById, deleteObjectById, getProperty

// ****************************************************************************************
//  TESTING

echo "<pre>";

foreach ($data as $i => $value) {
    $current_obj = new Admin($i + 1, $value['name'], $value['level']);
}

var_dump(Admin::getObjectById(3));

Admin::setObjectById(3, "name", "Hello");

var_dump(Admin::getObjectById(3));

