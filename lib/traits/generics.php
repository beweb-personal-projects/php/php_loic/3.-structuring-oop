<?php

trait Generics_Objects_Traits {
    protected static array $all_instanced_objects = [];

    public static function getAll(): array {
        return self::$all_instanced_objects;
    }

    public static function setAll(string $property, mixed $data): void {
        foreach(self::$all_instanced_objects as $i => $obj) {
            $obj->$property = $data;
        }
    }

    public static function deleteAll(): void {
        self::$all_instanced_objects = [];
    }

    public static function count(): int {
        return count(self::$all_instanced_objects);
    }

}

trait Generics_Object_Traits {
    
    public function get(): object {
        return $this;
    }

    public function getPropertyValue($property): mixed {
        return $this->$property;
    }

    public function set(string $property, mixed $data): void {
        $this->$property = $data;
    }

    public static function getObjectById(int $id): mixed {
        foreach (self::$all_instanced_objects as $i => $obj) {
            if((int)$obj->getPropertyValue("id") === $id) {
                return $obj;
            }
        }
        return null;
    }

    public static function setObjectById(int $id, string $property, mixed $data): void {
        foreach (self::$all_instanced_objects as $i => $obj) {
            if((int)$obj->getPropertyValue("id") === $id) {
                $obj->set($property, $data);
            }
        }
    }
}