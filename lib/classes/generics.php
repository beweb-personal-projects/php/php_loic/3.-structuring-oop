<?php
require(ROOT.'/lib/traits/generics.php');
require(ROOT.'/lib/interfaces/generics.php');

abstract class GenericsLibrary implements Generics_Objects, Generics_Object {
    use Generics_Objects_Traits, Generics_Object_Traits;
}