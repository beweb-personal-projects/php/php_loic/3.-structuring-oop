<?php 

interface Generics_Objects {
    public static function getAll(): array;
    public static function setAll(string $property, mixed $data): void;
    public static function deleteAll(): void;
    public static function count(): int;
}

interface Generics_Object {
    public function get(): object;
    public function getPropertyValue(string $property): mixed;
    public function set(string $property, mixed $data): void;
    public static function getObjectById(int $id): mixed;
    public static function setObjectById(int $id, string $property, mixed $data): void;
}